use serde_derive::Serialize;

pub struct DictionaryEntry {
    pub secwepemc: String,
    pub english: String,
    pub normalized_secwepemc: String,
    pub normalized_english: String,
}

#[derive(Debug, Serialize)]
pub struct DictionaryEntryDto {
    secwepemc: String,
    english: String,
}

impl DictionaryEntryDto {
    pub fn new(secwepemc: String, english: String) -> DictionaryEntryDto {
        DictionaryEntryDto { secwepemc, english }
    }
}

#[derive(Debug, Serialize)]
pub struct DictionarySearchResult {
    api_version: u8,
    entries: Vec<DictionaryEntryDto>,
}

impl DictionarySearchResult {
    pub fn new(api_version: u8, entries: Vec<DictionaryEntryDto>) -> DictionarySearchResult {
        DictionarySearchResult {
            api_version,
            entries,
        }
    }
}
